package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
	"unicode/utf8"

	"cloud.google.com/go/firestore"
	"golang.org/x/net/context"
	"google.golang.org/api/iterator"
	"google.golang.org/api/option"

	firebase "firebase.google.com/go"
	//"firebase.google.com/go/auth"
)

//Post to a topic in the discussion forum, struct for creating new post/ reply in the database
type Post struct {
	Topic   string    `json:"topic,omitempty"`
	Title   string    `json:"title,omitempty"`
	Content string    `json:"content,omitempty"`
	Author  string    `json:"author,omitempty"`
	Post    bool      `json:"Post,omitempty"`
	Time    time.Time `json:"time,omitempty"`
	//ReplyKey string    `json:"replyKey,omitempty"`
}

func handlerDiscussionForum(w http.ResponseWriter, r *http.Request) {

	keyFilepath := os.Getenv("KEY_FILEPATH")
	ctx := context.Background()
	var post Post

	opt := option.WithCredentialsFile(keyFilepath)
	app, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		log.Fatalf("error initializing app: %v\n", err)
	}

	client, err := app.Firestore(ctx)
	if err != nil {
		log.Fatal(err)
	}

	if origin := r.Header.Get("Origin"); origin != "" {
		w.Header().Set("Access-Control-Allow-Origin", "https://thebestdiscussionforuminntnu.netlify.com")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET")
	}

	switch r.Method {
	case "OPTIONS":
		return

	case "POST":
		////////////////////	Send json body of data to the database	////////////////////

		//	read the sent body of data
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Println("getRequestBody error in post. ") //TODO exchange with proper http error
			status := http.StatusBadRequest
			http.Error(w, http.StatusText(status), status)
			return
		}
		json.Unmarshal(body, &post)
		//fmt.Fprintln(w, string(body))

		//	check that all required fields have content
		if post.Topic == "" || post.Author == "" || post.Content == "" || post.Title == "" {
			fmt.Fprintln(w, "\n\n---------Topic: "+post.Topic)
			fmt.Fprintln(w, "\n---------Author: "+post.Author)
			fmt.Fprintln(w, "\n---------Content: "+post.Content)
			fmt.Fprintln(w, "\n---------Title: "+post.Title)
			status := http.StatusBadRequest
			http.Error(w, "Empty fields not permitted: ---"+http.StatusText(status), status)
			return
		}

		err = sendPostToDB(post, client, w)
		if err != nil {
			status := http.StatusBadRequest
			http.Error(w, http.StatusText(status), status)
			return
		}
		status := http.StatusOK
		http.Error(w, http.StatusText(status), status)
		break
	case "GET":
		var replies Post
		var replyMap []string
		URL := r.URL.Path

		//	get the path to the posts/replies to be displayed
		postType := strings.Split(URL, "/")
		if len(postType) > 3 || (len(postType) == 3 && utf8.RuneCountInString(postType[2]) > 60) {

			http.Error(w, "URL incorrect", http.StatusBadRequest)
			return
		}

		//	Get the iterator from the path
		iter := client.Collection("Topics").Doc(postType[1]).Collection("Posts").Documents(ctx)
		if len(postType) == 2 {
			iter = client.Collection("Topics").Doc(postType[1]).Collection("Posts").Documents(ctx)
		} else if len(postType) == 3 {
			iter = client.Collection("Topics").Doc(postType[1]).Collection("Posts").Doc(postType[2]).Collection("Replies").Documents(ctx)
		} else {
			http.Error(w, "URL incorrect", http.StatusBadRequest)
			return
		}

		for {
			doc, err := iter.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				http.Error(w, "Error fetching information from database", http.StatusInternalServerError)
			}

			dataFromDatabase := doc.Data()
			//fmt.Fprintln(w, doc.Data())
			dataConvertedToJSON, err := json.Marshal(dataFromDatabase)
			if err != nil {
				panic(err)
			}
			//fmt.Fprintln(w, string(dataConvertedToJSON))
			json.Unmarshal(dataConvertedToJSON, &replies)
			replyMap = append(replyMap, string(dataConvertedToJSON))
		}
		//fmt.Fprintln(w, replyMap)
		sendReply, err := json.Marshal(replyMap)
		if err != nil {
			panic(err)
		}
		w.Write(sendReply)

		break

	default:
		http.Error(w, "Error: not implemented yet", http.StatusNotImplemented)
		return
	}
	defer client.Close()
}

func getRequestBody(repoPath string) []byte {
	resp, err := http.Get("https://glacial-sierra-11788.herokuapp.com/")
	if err != nil {
		log.Println("getRequestBody, response, error. ")
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("getRequestBody, response, error. ")
	}

	//TODO remove, debug testing
	//info := string(body)
	//log.Print(info)

	return body
}

func sendPostToDB(post Post, client *firestore.Client, w http.ResponseWriter) error {
	var postToSend Post
	var err error
	ctx := context.Background()

	//	Sanetizing input
	postToSend.Topic = string(post.Topic)
	postToSend.Title = string(post.Title)
	postToSend.Content = string(post.Content)
	postToSend.Author = string(post.Author)
	postToSend.Time = time.Time(time.Now())
	postToSend.Post = bool(post.Post)

	//fmt.Fprintln(w, postToSend.Post)
	//fmt.Fprintln(w, "^ --------- Post bool field for post---------")

	if post.Post /*&& postToSend.Reply != "true"*/ {
		_, err = client.Collection("Topics").Doc(postToSend.Topic).Collection("Posts").Doc(postToSend.Title).Set(ctx, postToSend)
		if err != nil {
			http.Error(w, "Error posting information to database", http.StatusInternalServerError)
		}

		//result, _ := json.Marshal(&postToSend)
		//fmt.Fprintln(w, string(result))
		//fmt.Fprintln(w, "^ Printed _post_ from sendPostToDB")

	} else if !post.Post /*&& postToSend.Reply == "true"*/ {
		_, err = client.Collection("Topics").Doc(postToSend.Topic).Collection("Posts").Doc(postToSend.Title).Collection("Replies").Doc(postToSend.Time.String()).Set(ctx, postToSend)
		if err != nil {
			http.Error(w, "Error posting information to database", http.StatusInternalServerError)
		}
		//result, _ := json.Marshal(&postToSend)
		//fmt.Fprintln(w, string(result))
		//fmt.Fprintln(w, "^ Printed _reply_ from sendPostToDB")

	} else {
		return err
	}
	w.Header().Set("Content-Type", "application/JSON")
	err = json.NewEncoder(w).Encode(post)
	if err != nil {
		return err
	}

	return err
}

func determineListenAddress() (string, error) {
	port := os.Getenv("PORT")
	if port == "" {
		return "", fmt.Errorf("$PORT not set")
	}
	return ":" + port, nil
}

func main() {
	addr, err := determineListenAddress()
	if err != nil {
		log.Fatal(err)
	}

	http.HandleFunc("/", handlerDiscussionForum)

	log.Printf("Listening on %s...\n", addr)
	if err := http.ListenAndServe(addr, nil); err != nil {
		panic(err)
	}
}
