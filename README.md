#	IMT3501 software security, assignment 2	

##	Authors
*	Nataniel Gåsøy (Back-end coder)
*	Christian Bråthen Tverberg (Front-end coder)
*	Maarten Dijkstra (Technical support)
*	Benjamin Normann Skinstad (Technical support)
*	Jens Fjellheim (Technical support)
*	Hugo Nørholm (Design)
*	Vegard Elgesem Kostveit (Design)

##	Description
For this task, we were to develop, implement and deploy an online discussion forum. The back-end code is deployed on Heroku, the data is stored in a Firebase database and the front-end is deployed using netlify. 

##	Back-end
This assignment has been deployed on heroku. In order to run it, either use the existing heroku deployment (https://glacial-sierra-11788.herokuapp.com), deploy it on your own heroku or modify it to suit you own deployment environment. NOTE: in order to deploy it yourself:
* you will need to modify the code to suit your deployment method .
* in order to run heroku locally, you will first need to download heroku, and make a ".env" file (PORT=5000 KEY_FILEPATH=<path to database access key>).
* you will need to set up a firebase database and include the path in heroku config vars (under settings in the heroku webpage) to your databse key, generated in firebase - settings - service accounts - firebase admin SDK (or another database if you prefer).

Clone code: git clone https://Arcarnan@bitbucket.org/Arcarnan/imt3501_software_security_assignment2.git

###	Execute back-end
* Download Go: $ sudo apt-get install golang-go
* Add the Firebase Admin SDK to your Go app: $go get firebase.google.com/go
* Change directory to where main.go is located.
* Run "PORT=5000 go run main.go" in terminal (make sure you are in the gopath directory).
* go to "localhost:5000/testTopic" in your browser of choice to check that you are running properly (one method to test backend without frontend is by using postman, remember to formulate your bodies as json).

##	Front-end
* Clone code: git clone https://Arcarnan@bitbucket.org/Arcarnan/imt3501_software_security_assignment2.git in this repo you will find the folder 'Front-End-Files' which are the necessary files for creating our front end.

###	Execute front-end
* Locate index.html, and double-click to open in your default browser.
* We used netlify to deploy the application online and not using it locally. It can be accessed through going to the website: 'https://thebestdiscussionforuminntnu.netlify.com'.
* At the moment sending requests to create/update/delete or log in a user may only be handled through our domain 'https://thebestdiscussionforuminntnu.netlify.com'. This is to make sure that the firebase configuration won't be misused and to maintain the integrity of the system, so if you want to test requests comprising authentication with your own domain please contact us with the domain you want to use so we can add it to the list of HTTP referrers where requests will be accpeted. (We wanted our data be secure and this way it will be.)
* For using netlify you need to register and login, and then you can simply drag and drop the folder with necessary files into the deployment tab, and wait for it to be published. Then you can visit 'https://domain-name-you-write-yourself.netlify.com'.
