/* This script has the functions:
** 1) getCategories()           -line 29
** 2) getPosts()                -line 56
** 3) sendCategoryName()        -line 110
** 4) getPostDescription()      -line 116
** 5) getReplies()              -line 169
** 6) sendNewReplyRequest()     -line 244
** 7) sendNewPostRequest()      -line 311

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

** 1) This function prints out categories we currently have in the forum in a button element.

** 2) This function collects posts from the database given the category pressed and prints them out in a button element.

** 3) This function sends the user to the page showing a post, considering the category and post names.

** 4) This function collects the text of a post after that post has been clicked. This in the post.html page with the replies showing below the post data.

** 5) This function collects replies from the database given the post pressed within a category and prints them out.

** 6) This function will collect the input the user wrote in some textboxes for creating a new post and will create a post if user is logged in and values are valid.

** 7) This function will collect the input the user wrote in some textboxes for creating a new reply and will create a reply is user is logged in and values are valid.
*/



function getCategories() {
    // Setup constant different categories we have in our forum:
    var data = new Array();
    data[0] = "Unity3D";
    data[1] = "Games";
    data[2] = "Music";
    data[3] = "Sports";
    data[4] = "Food";
    data[5] = "Funny";
    data[6] = "Memes";


    for (i = 0; (i < data.length); i++) {
        var li = document.createElement("button");                              // Create button item.
        li.style.class = "categories";
        li.classList.add("category-button");                                    // Setup style for the category.
        li.onclick = function() {
            sendCategoryName(this);
        }
        var t = document.createTextNode(data[i]);                               // Create TextNode with text.
        li.appendChild(t);                                                      // Put textnode in listitem.
        document.getElementById("category-list").appendChild(li);               // Add listitem to list.
    }
}


// This function will print out all of the posts within a category
function getPosts() {
    try {
            //Get topic user want to get posts for:
            var topic = window.location.search;
            if (topic.substring(0, 1) == '?') {
                topic = topic.substring(1);
            }

            var url = "https://glacial-sierra-11788.herokuapp.com/" + topic;
            var xhttp = new XMLHttpRequest();
            xhttp.open("GET", url, false);
            xhttp.setRequestHeader("Content-type", "application/JSON");
            xhttp.send();   // Send request to heroku:

            response = JSON.parse(xhttp.response);

		if (response != null) {   // Get request failed, or could not find posts in database.

	            for (i = 0; i < response.length; i++) {
	                post = JSON.parse(response[i]);

	                var li = document.createElement("button");                    // Create listitem
	                li.classList.add("post-button");                              // Setup style for the category.
                	li.onclick = function() {
	                    sendCategoryName(this);
	                }


	                // Create textnode with title and author:
	                var title = document.createElement("postTitle");
	                title.setAttribute("class","postTitle");
	                title.textContent = post.Author + " - " + post.Title;
	                li.appendChild(title);

	                var linebreak = document.createElement('br');
	                li.appendChild(linebreak);

	                // Create text for a post:
	                var text = document.createTextNode(post.Content);
	                if (text.length > 30) {
	                    text.nodeValue = text.nodeValue.substring(0, 25) + "...";
	                }
	                li.appendChild(text);

	                document.getElementById("post-list").appendChild(li);                   // Add listitem to list.

	            }
		}
        } catch (error) {       // Category most likely had no posts.
            document.write("Error getting posts");
        }
}

// Send post name of postbutton pressed to next html:
function sendCategoryName(element) {
    var text = element.childNodes[0].textContent;
    text = text.substring(text.indexOf(" - ") + 3); // Gets the title without the author of a post.
    sendData(text);
}

function getPostDescription() { // Get Content of a post!
    try {
        // Get topic user want to get posts for:
        var topic = window.location.search;
        if (topic.substring(0, 1) == '?') {
            topic = topic.substring(1);
        }
        topic = topic.substring(0, topic.indexOf("&"));

        var url = "https://glacial-sierra-11788.herokuapp.com/" + topic;
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", url, false);
        xhttp.setRequestHeader("Content-type", "application/JSON");
        xhttp.send();   // Send request to heroku:

        response = JSON.parse(xhttp.response);


        // Check the post the current user is trying to see:
        var post1 = window.location.search;
        while (post1.substring(0, 1) != '&') {  // Remove every character until & is found.
            post1 = post1.substring(1);
        }
        post1 = post1.substring(1);
        post1 = decodeURI(post1);

        for (i = 0; i < response.length; i++) { // Loop through until correct post has been found.
            post = JSON.parse(response[i]);

            // Post description is found:
            if (post.Title == post1) {
                document.getElementById('postTitle').innerHTML = post.Title;
                document.getElementById('postContent').innerHTML = post.Content;
            }
        }


    } catch (error) {
        console.log("Error - Could not find post.");
    }
}

/*function deleteReply(element) { // Apparently we were not gonna delete replies or edit replies in the forum, so we won't use any of these.
    try {
        console.log("text of button:");
        console.log(element.childNodes[0].textContent);
        console.log("parent name");
        console.log(element.parentElement.childNodes[0].textContent);
    } catch (error) {
        console.log(error.message);
    }
}*/

function getReplies() {
    try {
        getPostDescription();
        // Get topic user want to get posts for:
        var topic = window.location.search;
        if (topic.substring(0, 1) == '?') {
            topic = topic.substring(1);
        }
        topic = topic.substring(0, topic.indexOf("&"));


        // Get post user want to get posts for:
        var post = window.location.search;
        while (post.substring(0, 1) != '&') {  // Remove every character until & is found.
            post = post.substring(1);
        }
        post = post.substring(1);
        post = decodeURI(post);


        var url = "https://glacial-sierra-11788.herokuapp.com/" + topic + "/" + post;
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", url, false);
        xhttp.setRequestHeader("Content-type", "application/JSON");
        xhttp.send();   // Send request to heroku:

        response = JSON.parse(xhttp.response);

	    if (response != null) {

              for (i = 0; i < response.length; i++) {
	          reply = JSON.parse(response[i]);

        	  var li = document.createElement("button");                              // Create listitem
	          li.classList.add("reply-button");                                        // Setup style for the category.


	          // Create textnode for content:
	          var text = document.createTextNode(reply.Content);
	          li.appendChild(text);

	          var linebreak = document.createElement('br');
	          li.appendChild(linebreak);

              // Create textnode for author and time reply was made:
	          var title = document.createElement("replyAuthor");
	          title.setAttribute("class","replyAuthor");
	          var date = reply.Time.substring(0, 10);
	          var time = reply.Time.substring(11, 17);
	          title.textContent = reply.Author + " - " + date + " " + time;
	          li.appendChild(title);

              /*li.appendChild(linebreak);    // We don't have deleting replies implemented back-end.

              // Create button for deleting reply - if you are the owner of that reply.
              var button = document.createElement("deleteReplyButton");
              button.setAttribute("class", "deleteReplyButton");
              button.onclick = function() {
                    deleteReply(this);
                }
                button.textContent = "Delete";
                li.appendChild(button);*/


	          document.getElementById("reply-list").appendChild(li);                   // Add listitem to list.

         }
	}

    } catch (error) { // There was most likely no replies to that post.
        document.write(error.message);
    }
}

/*This function will collect the input the user wrote in some textboxes for creating a new reply and will create a reply is user is logged in and values are valid.*/
function sendNewReplyRequest() {

    const replyText = document.getElementById('replyText');
    const btnSendPost = document.getElementById('sendReplyButton');

    btnSendPost.addEventListener('click', e => {

        if (!firebase.auth().currentUser) {
            document.getElementById('postDebugText').innerHTML = "You must log in to create replies";
            console.log("You must log in to create replies");
        } else {

            // Here I will send a post request to heroku for creating a new post:
            var xhr = new XMLHttpRequest();
            xhr.open("POST", 'https://glacial-sierra-11788.herokuapp.com/test', true);

            // Send the proper header information along with the request
            xhr.setRequestHeader("Content-Type", "application/json");

            xhr.onreadystatechange = function() {//Call a function when the state changes.
                if(this.readyState == XMLHttpRequest.DONE && this.status == 200) {
                    // Request finished.
                    console.log("reply has been made");
                    location.reload();
                }
            }

            // Get the topic:
            var topic = window.location.search;
            if (topic.substring(0, 1) == '?') {
                topic = topic.substring(1);
            }
            topic = topic.substring(0, topic.indexOf("&"));

            // Get the postTitle:
            var postTitle = window.location.search;
            while (postTitle.substring(0, 1) != '&') {  // Remove every character until & is found.
                postTitle = postTitle.substring(1);
            }
            postTitle = postTitle.substring(1);
            postTitle = decodeURI(postTitle);

            // Body for post request:
            var Data={
                "topic": topic,
                "title": postTitle,
                "content": replyText.value,
                "author": firebase.auth().currentUser.displayName,
                "post": false
            };

            var postValue = JSON.stringify(Data);
            xhr.send(postValue);

            // Reset textbox values:
            replyText.value = "";
        }
    });
    firebase.auth().onAuthStateChanged(firebaseUser => {
        if (firebaseUser) {
            // If user logs in the Login error for trying to create posts will disappear:
            document.getElementById('postDebugText').innerHTML = "";
        }
    });
}

/*This function will collect the input the user wrote in some textboxes for creating a new post and will create a post if user is logged in and values are valid.*/
function sendNewPostRequest() {
    const postTitle = document.getElementById('postTitle');
    const postText = document.getElementById('postText');
    const btnSendPost = document.getElementById('sendPostButton');

    btnSendPost.addEventListener('click', e => {

        if (!firebase.auth().currentUser) {
            document.getElementById('postDebugText').innerHTML = "You must log in to create posts";
            console.log("You must log in to create posts");
        } else {
            // Some research apparently says that keeping title under 60 characters will make about 90% of titles to display properly, so we used this.
            if (postTitle.value.length < 60) {

                // Here I will send a post request to heroku for creating a new post:
                var xhr = new XMLHttpRequest();
                xhr.open("POST", 'https://glacial-sierra-11788.herokuapp.com/test', true);

                // Send the proper header information along with the request
                xhr.setRequestHeader("Content-Type", "application/json");

                xhr.onreadystatechange = function() {//Call a function when the state changes.
                    if(this.readyState == XMLHttpRequest.DONE && this.status == 200) {
                        // Request finished.
                        console.log("Created a new post!");
                        location.reload();
                    }
                }

                // Get the topic:
                var topic = window.location.search;
                if (topic.substring(0, 1) == '?') {
                    topic = topic.substring(1);
                }
                // Body for post request:
                var Data={
                    "topic": topic,
                    "title": postTitle.value,
                    "content": postText.value,
                    "author": firebase.auth().currentUser.displayName,
                    "post": true
                };

                var postValue = JSON.stringify(Data);
                xhr.send(postValue);


                // Reset textbox values:
                postTitle.value = "";
                postText.value = "";
                document.getElementById('postDebugText').innerHTML = "";
            } else {
                document.getElementById('postDebugText').innerHTML = "Slow down there, you should keep your title below 60 charracters.";
            }
        }
    });
    firebase.auth().onAuthStateChanged(firebaseUser => {
        if (firebaseUser) {
            // If user logs in the Login error for trying to create posts will disappear:
            document.getElementById('postDebugText').innerHTML = "";
        }
    });
}
