// This file contains functions for registering user and logging in user.
function login() {

    // Initialize Firebase
    // Config can stay public because it may only be used from our domain.(Domain can be changed in the database.)
    var config = {
      apiKey: "AIzaSyBfh2YxQD4fKGfhTFzlB-lHml7_QO5jhpg",
      authDomain: "imt3501-softsec-assignment2.firebaseapp.com",
      databaseURL: "https://imt3501-softsec-assignment2.firebaseio.com",
      projectId: "imt3501-softsec-assignment2",
      storageBucket: "imt3501-softsec-assignment2.appspot.com",
      messagingSenderId: "677105312642"
    };
    firebase.initializeApp(config);


        // Get elements:
    const txtEmail = document.getElementById('txtEmail');
    const txtPassword = document.getElementById('txtPassword');
    const btnLogin = document.getElementById('btnLogin');
    const btnLogout = document.getElementById('btnLogout');


        // Sign in a user if credentials are correct:
    btnLogin.addEventListener('click', e => {
        // Get email and password
        const email = txtEmail.value;
        const pass = txtPassword.value;
        const auth = firebase.auth();

        if (pass.length < 4 || email.length < 4) {
            document.getElementById('debugText').innerHTML = "Email or Password is not correct";
        } else {
            // Sign in:
            document.getElementById('debugText').innerHTML = "";
            const promise = auth.signInWithEmailAndPassword(email, pass);   // If there is no error -> no error message will show and the window display will hide some buttons etc.
            promise.catch(function(error) {
                console.log(error);
                if (error.code = "auth/user-not-found") {           // A popup will show, letting the user create a new account.
                    var popup = document.getElementById("myPopup");
                    popup.classList.toggle("show");
                } else if (error.code = "auth/wrong-password") {
                    document.getElementById('debugText').innerHTML = "Email or password is not correct";
                }
            });
        }
    });

    // Sign out a user:
    btnLogout.addEventListener('click', e => {
        firebase.auth().signOut();
    })

    // The moment a user is loggin off or loggin in some stuff will happen:
    firebase.auth().onAuthStateChanged(firebaseUser => {
        if (firebaseUser) {
            console.log("Logging in");
            txtEmail.style.display = "none";
            txtPassword.style.display = "none";
            btnLogin.style.display = "none";
            btnLogout.style.display = "inline-block";
        } else {
            console.log("Loggin out");
            txtEmail.style.display = "inline-block";
            txtPassword.style.display = "inline-block";
            btnLogin.style.display = "inline-block";
            btnLogout.style.display = "none";
        }
    });

};




    // This function will register a user:
function registerUser() {

    // Initialize Firebase
    var config = {
      apiKey: "AIzaSyBfh2YxQD4fKGfhTFzlB-lHml7_QO5jhpg",
      authDomain: "imt3501-softsec-assignment2.firebaseapp.com",
      databaseURL: "https://imt3501-softsec-assignment2.firebaseio.com",
      projectId: "imt3501-softsec-assignment2",
      storageBucket: "imt3501-softsec-assignment2.appspot.com",
      messagingSenderId: "677105312642"
    };
    firebase.initializeApp(config);


    // Get elements:
    const userName = document.getElementById('userName');
    const txtEmail = document.getElementById('txtEmail');
    const txtPassword = document.getElementById('txtPassword');
    const btnSignUp = document.getElementById('btnSignUp');



    // Sign up a new user for the forum if credentials are accepted:
    btnSignUp.addEventListener('click', e => {
        const email = txtEmail.value;
        const pass = txtPassword.value;
        const auth = firebase.auth();

        if (email.length < 4) {
            document.getElementById('debugText').innerHTML = "Email is not valid";
        } else {
            if (pass.length < 4) {
                document.getElementById('debugText').innerHTML = "Password is not long enough";
            } else {
                // Sign in:
                document.getElementById('debugText').innerHTML = "";

                    // Some checking of email and password will be done through firebase to make sure that email and password are valid.
                const promise = auth.createUserWithEmailAndPassword(email, pass);
                promise.catch(function(error) {
                    document.getElementById('debugText').innerHTML = "An user with that email already exists";
                });
                // Set userName for new user in forum.
                var user = firebase.auth().currentUser;

                user.updateProfile({
                    displayName: userName.value
                })
                .then(function() {
                    console.log("userName set!");
                    window.location = "index.html";
                })
                .catch(function(error) {
                    console.log(error);
                });
            }
        }
    });
}
